# Estilo de Publicación con MarkDown

Chuletario y guía funcional de uso del lenguaje de etiquetado para la redacción de documentación y noticias

Para escribir en archivos de texto con formato .md, se usan las reglas que puedes ver aquí:  
[Tutorial de Markdown](https://joedicastro.com/pages/markdown.html "Tutorial MarkDown")

Las más importantes son:
- \# Esto es un H1
- \[link con titulo](http://joedicastro.com "pop-up")
- \![Imagen con titulo] (pictures/avatar.png "pop-up")
- \`codigo`
- \**Cursiva**
- Dos espacios ==> Salto de línea
- \*** ==> linea divisioria
- \ ==> para escapar de los carácteres de markdown
1. ==> Lista
- \- ==> lista no numerada
- \> ==> nota
- \```json <br>```
- <details>
  <summary>contenido expansivo:</summary>

  ```html
  <details>
    <summary>contenido expansivo:</summary>
    1. A numbered list
      * With some Sub bullets
  </details>
  ```
  </details>
- Hacer referencia a una parte del documento: añade `#` tras el link +  el título del apartado, así: https://gitlab.com/detailorg/backend/cryptorelay/-/blob/master/docs/deploy.md#crontab